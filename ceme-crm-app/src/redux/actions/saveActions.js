import actions from "./actionTypes";
import axios from "axios";

export const saveCustomersSuccess = (response) => {
	console.log("saveCustomersSuccess", response);
	return {
		type: actions.SAVE_CUSTOMER_SUCCESS,
		payload: response,
	};
};

export const saveCustomerFailure = (err, customerSearch) => {
	return {
		type: actions.SAVE_CUSTOMER_FAILURE,
		payload: { message: `Save customers ${customerSearch} failed` },
	};
};

export const saveCustomer = (customer) => {
	console.log(customer);
	return (dispatch, getStatus) => {
		axios.post(`http://localhost:8080/api/customer/save`, customer).then(
			(res) => {
				dispatch(saveCustomersSuccess(true));
			},
			(err) => {
				dispatch(saveCustomerFailure(err));
			}
		);
	};
};
