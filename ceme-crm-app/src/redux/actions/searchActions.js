import searchActions from "./searchActionTypes";
import axios from "axios";

export const searchCustomerBegin = (customerSearch) => {
	console.log("Begin by Type", customerSearch);
	return {
		type: searchActions.SEARCH_CUSTOMER_BEGIN,
		payload: `Search By ${customerSearch} begin`,
	};
};

export const searchCustomersSuccess = (response, searchText, searchType) => {
	console.log("searchCustomersSuccess", response);
	return {
		type: searchActions.SEARCH_CUSTOMER_SUCCESS,
		payload: {
			customers: response,
			searchText: searchText,
			searchType: searchType,
			isSuccess: true,
			error: {},
		},
	};
};

export const searchCustomerFailure = (error, searchText, searchType) => {
	return {
		type: searchActions.SEARCH_CUSTOMER_FAILURE,
		payload: {
			customers: [],
			error: error,
			searchText: searchText,
			searchType: searchType,
			isSuccess: false,
		},
	};
};

export const searchCustomers = (searchText, searchType) => {
	return async (dispatch, getState) => {
		dispatch(searchCustomerBegin(searchType));
		return await axios
			.get(
				`http://localhost:8080/crm/customersearch/${searchType}/${searchText}`
			)
			.then(
				(res) => {
					dispatch(searchCustomersSuccess(res.data, searchText, searchType));
					return true;
				},
				(err) => {
					let error;
					if (err.response && err.response.data) {
						error = err.response.data;
					} else {
						error = {
							message: "Failed to fetch customers.. please try again later",
						};
					}
					dispatch(searchCustomerFailure(error, searchText, searchType));
					return false;
				}
			);
	};
};

export const setSelectedCustomer = (customer) => {
	return {
		type: searchActions.SELECTED_CUSTOMER_IN_SEARCH,
		payload:  customer,
	};
};
