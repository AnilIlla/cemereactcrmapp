import custinteractionTypes from "./actionTypes";
import axios from "axios";

export const getCustomerInteractionBegin = () => {
	console.log("Begin customer interac search");
	return {
		type: custinteractionTypes.GET_INTERACTION_DETAIL_BEGIN,
	};
};

export const getCustomerInteractionSuccess = (response) => {
	console.log("getCustomerInteraction", response);
	return {
		type: custinteractionTypes.GET_INTERACTION_DETAIL_SUCCESS,
		payload: response ,
	};
};

export const getCustomerInteractionFailure = (err) => {
	return {
		type: custinteractionTypes.GET_INTERACTION_DETAIL_FAILURE,
		payload: { message: `Fetching customers interactions failed` },
	};
};

export const getCustomerInteractionDetail = (custId) => {
	console.log("cust id value", custId);
	return (dispatch, getStatus) => {
		dispatch(getCustomerInteractionBegin());
		axios
			.get(
				`http://localhost:8080/api/customer/interaction/${custId}`
			)
			.then(
				(res) => {
					return dispatch(getCustomerInteractionSuccess(res.data));
					
				},
				(err) => {
					 dispatch(getCustomerInteractionFailure(err));
				}
			);
	};
};

