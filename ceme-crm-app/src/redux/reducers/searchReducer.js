import searchActions from "../../redux/actions/searchActionTypes";

export const CRM_REDUCER_INITIAL_STATE = {
	data: {
		customers: [],
		searchText: "",
		searchType: "",
		isSuccess: false,
		error: {},
	},
	customer: {},
	isSearchedButtonClicked: false,
	loading: false,
};

const searchReducer = (state = CRM_REDUCER_INITIAL_STATE, action) => {
	switch (action.type) {
		case searchActions.SEARCH_CUSTOMER_BEGIN:
			return {
				...state,
				loading: true,
			};
		case searchActions.SEARCH_CUSTOMER_SUCCESS:
			return {
				...state,
				data: action.payload,
				isSearchedButtonClicked: true,
				loading: false,
			};
		case searchActions.SEARCH_CUSTOMER_FAILURE:
			return {
				...state,
				data: action.payload,
				isSearchedButtonClicked: false,
				loading: false,
			};
		case searchActions.SELECTED_CUSTOMER_IN_SEARCH: {
			return {
				...state,
				customer: action.payload,
				isSearchedButtonClicked: false,
			};
		}
		default: {
			return state;
		}
	}
};

export default searchReducer;
