import React from "react";
import ReactDOM from "react-dom";
import App from "./components/App";
import { BrowserRouter as Router } from "react-router-dom";
import { Provider } from "react-redux";
import appStore from "./redux/index";
ReactDOM.render(
  <Router>
    <Provider store={appStore}>
      <App />
    </Provider>
  </Router>,
  document.getElementById("root")
);
