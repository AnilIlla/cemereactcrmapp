import React from "react";

const GreetingCustomer = (props) => {
	console.log("greeting", props.customer);
	const greet = () => {
		var today = new Date();
		var curHr = today.getHours();
		if (curHr < 12) {
			return "Good Morning";
		} else if (curHr < 18) {
			return "Good Noon";
		} else {
			return "Good Evening";
		}
	};
	return (
		<React.Fragment>
			{Object.getOwnPropertyNames(props.customer).length > 0 && (
				<div className="card">
					<div className="card-header greeting_message">
						{`${greet()} ${props.customer.lastName}, ${
							props.customer.firstName
						}  we are happy to serve you`}
					</div>
				</div>
			)}
		</React.Fragment>
	);
};

export default GreetingCustomer;
