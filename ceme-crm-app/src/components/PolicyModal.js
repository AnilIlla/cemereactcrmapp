import React, { useState, useEffect } from "react";

import { Form, Button, Col, Modal } from "react-bootstrap";
// import { useSelector } from "react-redux";

const PolicyModal = (props) => {
	const [show, setShow] = useState(false);
	const [validated, setValidated] = useState(false);

	// console.log(props);
	const policyObj = {
		id: 0,
		custId: 0,
		policyNumber: "",
		premiumAmount: "",
		effectiveDate: "",
		productType: "",
		company: "Allstate",
		status: "Active",
		terminationReason: "NA",
	};

	const [policy, setPolicy] = useState(policyObj);

	const handleClose = () => setShow(false);

	const handleShow = () => {
		if (!props.isEdit) {
			setPolicy(policyObj);
		} else {
			setPolicy(props.policy);
		}

		setShow(true);
		setValidated(false);
	};

	const handleSubmit = (event) => {
		const form = event.currentTarget;

		if (form.form.checkValidity() === false) {
			event.preventDefault();
			event.stopPropagation();
		} else {
			handleClose();
			console.log(policy);
			props.onSavePolicy(policy);
		}
		setValidated(true);
	};

	const changeHandler = (event) => {
		setPolicy({ ...policy, [event.target.name]: event.target.value });
	};

	// useEffect(() => {
	// 	handleShow
	// }, [props.isEdit]);

	return (
		<>
			{!props.isEdit && (
				<Button variant="primary" onClick={handleShow}>
					Add Policy
				</Button>
			)}
			{props.isEdit && (
				<input type="button" value="edit" onClick={handleShow} />
			)}
			<Modal show={show} onHide={handleClose}>
				<Modal.Header closeButton>
					<Modal.Title>Policy Information</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Form noValidate validated={validated}>
						<Form.Row>
							<Form.Group
								as={Col}
								md="6"
								controlId="policyNumber"
								className="text-left"
							>
								<Form.Label>Policy Number</Form.Label>
								<Form.Control
									required
									type="text"
									name="policyNumber"
									placeholder="Policy Number"
									value={policy.policyNumber}
									pattern="[0-9]*"
									minLength="5"
									maxLength="15"
									onChange={(e) => changeHandler(e)}
								/>
								<Form.Control.Feedback type="invalid">
									Please enter a valid policy number
								</Form.Control.Feedback>
							</Form.Group>
							<Form.Group
								as={Col}
								md="4"
								controlId="premiumAmount"
								className="text-left"
							>
								<Form.Label>Premium</Form.Label>
								<Form.Control
									required
									type="text"
									name="premiumAmount"
									minLength="3"
									maxLength="5"
									placeholder="Premium Amount"
									pattern="[0-9]*"
									value={policy.premiumAmount}
									onChange={(e) => changeHandler(e)}
								/>
								<Form.Control.Feedback type="invalid">
									Please enter a valid amount
								</Form.Control.Feedback>
							</Form.Group>
						</Form.Row>
						<Form.Row>
							<Form.Group
								as={Col}
								md="6"
								controlId="effectiveDate"
								className="text-left"
							>
								<Form.Label>Effective Date</Form.Label>
								<Form.Control
									type="date"
									name="effectiveDate"
									placeholder="Policy start date"
									value={policy.effectiveDate}
									onChange={(e) => changeHandler(e)}
								/>

								<Form.Control.Feedback type="invalid">
									Please enter a valid date
								</Form.Control.Feedback>
							</Form.Group>
							<Form.Group
								as={Col}
								md="4"
								controlId="productType"
								className="text-left"
							>
								<Form.Label>Policy Type</Form.Label>
								<Form.Control
									as="select"
									name="productType"
									type="l"
									placeholder="product"
									required
									pattern="[a-zA-Z]"
									value={policy.productType}
									onChange={(e) => changeHandler(e)}
								>
									<option value="">Select</option>
									<option value="auto">Auto</option>
									<option value="home">Home</option>
									<option value="renters">Renters</option>
								</Form.Control>
								<Form.Control.Feedback type="invalid">
									Please select a valid product
								</Form.Control.Feedback>
							</Form.Group>
						</Form.Row>
						<Form.Row>
							<Form.Group
								as={Col}
								md="2"
								controlId="close"
								className="text-left"
							>
								<Button variant="secondary" onClick={handleClose}>
									Close
								</Button>
							</Form.Group>
							<Form.Group
								as={Col}
								md="24"
								controlId="submit"
								className="text-left"
							>
								<Button variant="primary" onClick={handleSubmit}>
									Save Policy
								</Button>
							</Form.Group>
						</Form.Row>
					</Form>
				</Modal.Body>
				<Modal.Footer></Modal.Footer>
			</Modal>
		</>
	);
};

export default PolicyModal;
