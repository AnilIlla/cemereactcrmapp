import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  Accordion,
  Typography,
  AccordionSummary,
  AccordionDetails,
  AccordionActions,
  Button,
} from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import AccountCircle from "@material-ui/icons/AccountCircle";
import Divider from "@material-ui/core/Divider";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  heading: {
    fontSize: "1.00rem",
    fontWeight: "bold",
    color: "#0033a0",
    marginLeft: "10px",
  },
  sub_heading: {
    fontSize: "0.8rem",
    fontWeight: "bold",
    marginLeft: "10px",
    color: "#0033a0",
  },
}));

const SearchCustomerAccordion = (props) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Accordion
        style={{
          backgroundColor: "#e9ecef",
          borderBottom: "1px solid rgba(0,0,0,.125)",
        }}
      >
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
          className="mt-2 col-md-12"
        >
          <AccountCircle />
          <Typography className={classes.heading}>
            {props.customer.lastName}, {props.customer.firstName}
          </Typography>
          <Typography className={classes.heading}>
            {props.customer.city}
          </Typography>
        </AccordionSummary>
        <Divider />
        <AccordionDetails style={{ display: "block" }}>
          <Typography className="col-8 order-2 order-md-1">
            <span className={classes.sub_heading}>Age:</span>{" "}
            {props.customer.age}
          </Typography>
          <Typography className="col-8 order-2 order-md-1">
            <span className={classes.sub_heading}>City, State, Zip:</span>{" "}
            {props.customer.city}, {props.customer.state}, {props.customer.zip}
          </Typography>
          <Typography className="col-8 order-2 order-md-1">
            <span className={classes.sub_heading}>Email:</span>{" "}
            {props.customer.email}
          </Typography>
          <Typography className="col-8 order-2 order-md-1">
            <span className={classes.sub_heading}>Phone:</span>{" "}
            {props.customer.phone}
          </Typography>
        </AccordionDetails>
        <AccordionActions>
          <Button
            size="small"
            variant="contained"
            color="primary"
            onClick={() => props.redirectHandler(props.customer)}
          >
            View Details
          </Button>
        </AccordionActions>
      </Accordion>
    </div>
  );
};
export default SearchCustomerAccordion;
